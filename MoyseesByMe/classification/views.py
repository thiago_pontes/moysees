from django.http import HttpResponse
from django.template import loader
from django.contrib.auth import authenticate, login, logout
from django.db import transaction, IntegrityError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Product, ProductPriority
    
def index(request):            
    template = loader.get_template('classification/index.html')
    return HttpResponse(template.render({}, request))
    
def login_view(request):    
    temp_login = loader.get_template('classification/login.html')
    temp_home = loader.get_template('classification/user_home.html')

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)        
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponse(temp_home.render({}, request))
            else:
                return HttpResponse("Your Moysèés account is disabled.")
        else:
            return HttpResponse("Invalid login.")

    else:
        if not request.user.is_authenticated():
            return HttpResponse(temp_login.render({}, request))
        else:
            return HttpResponse(temp_home.render({}, request))
    
def logout_view(request):
    logout(request)
    template = loader.get_template('classification/index.html')
    return HttpResponse(template.render({}, request))
  
def user_home(request):    
    if not request.user.is_authenticated():
        template = loader.get_template('classification/index.html')
        return HttpResponse(template.render({}, request))
    else:    
        template = loader.get_template('classification/user_home.html')
        return HttpResponse(template.render({}, request))

def product_list(request):    
    product_list = Product.objects.order_by('name').all()
    template = loader.get_template('classification/product_list.html')
    
    paginator = Paginator(product_list,20)
    
    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)
    
    context = {
        'products': products,
    }
        
    return HttpResponse(template.render(context, request))

def insert_priority_list(request):     
    
    items_per_page = 20    
    
    if request.method == 'POST':
        
        if not request.user.is_authenticated():
            template = loader.get_template('classification/index.html')
            return HttpResponse(template.render({}, request))
        else:
            post_items = request.POST.get('num_items')
            session_items = request.session.get('num_items')
            page_button = request.POST.get('page')
            action_button = request.POST.get('action')
            
            if action_button == 'submit':
                complete_checked_list = request.session.get('complete_checked_list')
                if not complete_checked_list:
                    complete_checked_list = request.POST.getlist('product')
                 
                if (not complete_checked_list) or (len(complete_checked_list) == 0):
                    request.session['complete_checked_list'] = []
                    return HttpResponse("No products selected.")
                else: 
                    print(complete_checked_list)
                    try:               
                        http_response = insert_priorities(complete_checked_list,request)
                        request.session['complete_checked_list'] = []
                        return http_response
                    except Exception as e:
                        return HttpResponse("Sorry, nothing was saved. Shame on me! (%$)" % e.__cause__())
            elif post_items:                
                items = int(post_items)
                request.session['num_items'] = items
            elif session_items:
                items = int(session_items)
            else:
                return HttpResponse("Sorry! Shame on me!")
            
            prioritized_list = ProductPriority.objects.all().values('product')
            product_list = Product.objects.exclude(id__in=prioritized_list).order_by('name')[:items]
            
            if page_button == 'prev':
                page = int(request.POST.get('prev'))
                prev_page = page + 1
                complete_previous_page_list=list(str(o.id) for o in product_list)[(prev_page-1)*items_per_page:items_per_page*prev_page-1]
            elif page_button == 'next':
                page = int(request.POST.get('next')) 
                prev_page = page - 1 
                complete_previous_page_list=list(str(o.id) for o in product_list)[(prev_page-1)*items_per_page:items_per_page*prev_page-1] 
            else: 
                page = 1
                complete_previous_page_list = []  
            
            complete_actual_page_list=list(str(o.id) for o in product_list)[(page-1)*items_per_page:items_per_page*page-1]
                
            new_page_checked_list = request.POST.getlist('product') 
            if not new_page_checked_list:
                new_page_checked_list = []
    
            complete_checked_list = request.session.get('complete_checked_list')            
            
            if complete_checked_list == None:
                complete_checked_list = new_page_checked_list
            else:       
                not_checked_page_set = set(complete_previous_page_list) - set(new_page_checked_list)               
                unchecked_set = set(complete_checked_list) & not_checked_page_set
                previous_checked_set = set(complete_checked_list) - unchecked_set                
                complete_checked_list = list(previous_checked_set | set(new_page_checked_list))
            
            request.session['complete_checked_list'] = complete_checked_list
                       
            page_already_checked_list = list(int(a) for a in set(set(complete_actual_page_list) & set(complete_checked_list)))
        
                  
            paginator = Paginator(product_list,items_per_page)
            
            try:
                products = paginator.page(page)
            except PageNotAnInteger:
                products = paginator.page(1)
            except EmptyPage:
                products = paginator.page(paginator.num_pages)
            
            context = {
                'products': products, 'checked_list': page_already_checked_list,
            }
            
            template = loader.get_template('classification/insert_priorities.html')                
            return HttpResponse(template.render(context, request))        

def insert_priorities(items_list, request):
    
    user = request.user
    
    items_already_prioritized = 0
    items_prioritized = 0
    
    with transaction.atomic():
        for item in items_list:
            try:
                product = Product.objects.get(pk=item)
                pp = ProductPriority(user=user, product=product)
                pp.save()
                items_prioritized+=1
            except IntegrityError:
                items_already_prioritized+=1        
                
    return HttpResponse("%s products were classified by %s. %s products have already been classified" % (items_prioritized,user.get_username(),items_already_prioritized))
    
def user_priority_list(request):    
    product_list = ProductPriority.objects.filter(user=request.user.id).select_related('product').order_by('product__name')
    template = loader.get_template('classification/user_priority_list.html')    
    paginator = Paginator(product_list,20)
    
    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)
    
    context = {
        'products': products,
    }

    return HttpResponse(template.render(context, request))
    