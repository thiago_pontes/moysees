from django.db import models
from django.conf.global_settings import AUTH_USER_MODEL

# Create your models here.

class Product(models.Model):
    name = models.TextField(max_length=500)
    
    class Meta:
        managed = False
        db_table = 'product'
    
#class User(models.Model):
#    name = models.TextField(max_length=200)
#    password = models.TextField(max_length=10)
    
class ProductPriority(models.Model):
    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE)
    product = models.OneToOneField(Product)
    
    class Meta:
        db_table = 'product_priority'
    
    