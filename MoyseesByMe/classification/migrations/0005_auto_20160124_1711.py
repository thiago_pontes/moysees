# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-24 17:11
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('classification', '0004_auto_20160124_1710'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productpriority',
            name='product',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='classification.Product'),
        ),
    ]
