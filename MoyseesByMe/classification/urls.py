'''
Created on 22/01/2016

@author: thiago
'''
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login/$', views.login_view, name='login'),
    url(r'^$', views.index, name='index'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^product_list/$', views.product_list, name='product_list'),
    url(r'^insert_priority_list/$', views.insert_priority_list, name='insert_priority_list'),
    url(r'^insert_priorities/$', views.insert_priorities, name='insert_priorities'),
    url(r'^user_priority_list/$', views.user_priority_list, name='user_priority_list'),
]